﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import synocontrols as syno
from time import sleep

license = [
	'synocontrols - Python module to control LEDs and more on Synology-NAS',
	'Copyright (C) 2015 Alexander Knoop',
	'This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.',
	'This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.'
	]
for line in license:
	print(line)


print 'Test synocontrols'

sleep(3)

print 'Power: Off'
syno.power('off')
sleep(3)

print 'Power: Blink'
syno.power('blink')
sleep(3)

print 'Power: On'
syno.power('on')
sleep(3)

print 'Beep: Short'
syno.beep('short')
sleep(3)

print 'Beep: Long'
syno.beep('long')
sleep(3)

print 'Beep: double short'
syno.beep('dshort')
sleep(3)

print 'Beep: double long'
syno.beep('dlong')
sleep(3)

print 'Alarm'
syno.alarm(3)
sleep(5)

print 'Status: Off'
syno.status('off')
sleep(3)

print 'Status: Blink (Orange)'
syno.status('oblink')
sleep(3)

print 'Status: On (Orange)'
syno.status('oon')
sleep(3)

print 'Status: Blink (Green)'
syno.status('gblink')
sleep(3)

print 'Status: On (Green)'
syno.status('gon')
sleep(3)

print 'Copy: Blink'
syno.copy('blink')
sleep(3)

print 'Copy: On'
syno.copy('on')
sleep(3)

print 'Copy: Off'
syno.copy('off')
sleep(3)

print 'Test finished'
syno.beep('short')