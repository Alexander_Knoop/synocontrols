#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from setuptools import setup

with open('README.md') as f:
	readme = f.read()

setup(
	name='synocontrols',
	version='1.1',
	description='Python module to control LEDs and more on Synology-NAS.',
	url='https://bitbucket.org/Alexander_Knoop/synocontrols',
	author='Alexander Knoop',
	author_email='computer.online@web.de',
	py_modules=['synocontrols'],
	license='GPL-3.0',
	long_description=readme,
	)
