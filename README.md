# synocontrols #

This is a simple python module to control the LEDs of a Synology NAS. You can also send messages to users and make the system beep.

## Installation ##

If you have installed pip, you can install the module by downloading the .zip file under /dist/ and execute 

`pip install ./synocontrols-X.x.zip`

Otherwise you can simply copy the synocontrols.py file to your project folder. 

Have a look at the examples to see how easy it is to use this module. 

## Usage ##

To use the following functions, your python program has to run with root-privileges!

Keep in mind, that not every NAS supports all functions and that a NAS is no party machine! **I am not responsible for any damage of LEDs or other parts of the NAS or any other hardware!**

### LED-Control ###

To control the LEDs of the system, you can simply use the following functions:

```
#!python

synocontrols.power(arg)
```

Control the power LED. Possible arguments are:

Argument | Action
-------- | ------
on | Turn the LED on
off | Turn the LED off
blink | Let the LED blink


```
#!python

synocontrols.status(arg)
```

Control the status LED. Possible arguments are:

Argument | Action
-------- | ------
off | Turn the LED off
gon | Turn the LED on (green)
gblink | Let the LED blink (green)
oon | Turn the LED on (orange)
oblink | Let the LED blink (orange)


```
#!python

synocontrols.copy(arg)
```

Control the copy LED (only on some NAS). Possible arguments are:

Argument | Action
-------- | ------
off | Turn the LED off
on | Turn the LED on
blink | Let the LED blink

### Beep-Control ###

The system has an build in speaker which can be used for acoustic information.

```
#!python

synocontrols.beep(arg, [times])
```

Argument | Action
-------- | ------
short | Make a short beep
long | Make a long beep

Times defines how many beeps should be played. Default is 1. Note that if you define a value > 1 the function will pause your program till all beeps were played.

```
#!python

synocontrols.beep2(arg, active, [times])
```

This is the same as using the function above, but the system only beeps, if the active-Argument is true. This makes it easy to turn audio-feedback on and off.


```
#!python

synocontrols.alarm(duration)
```

Starts the alarm of the system (beeping and orange blinking of status and hdd LEDs) for the given duration in seconds.

### Send messages ###

You can use this module to send messages to the DSM-Desktop (like messages of firmware updates or completed backups).

```
#!python

synocontrols.send_message(title, text)
```

This sends a message to every user of the administrators-group. Messages with same title are grouped.

```
#!python

synocontrols.send_message2(title, text, group)
```

Using this command, you can specify the group to send the message to.